%{!?_httpd_mmn: %{expand: %%global _httpd_mmn %%(cat %{_includedir}/httpd/.mmn || echo 0-0)}}
%{!?_httpd_apxs:       %{expand: %%global _httpd_apxs       %%{_sbindir}/apxs}}
%{!?_httpd_confdir:    %{expand: %%global _httpd_confdir    %%{_sysconfdir}/httpd/conf.d}}
# /etc/httpd/conf.d with httpd < 2.4 and defined as /etc/httpd/conf.modules.d with httpd >= 2.4
%{!?_httpd_modconfdir: %{expand: %%global _httpd_modconfdir %%{_sysconfdir}/httpd/conf.d}}
%{!?_httpd_moddir:    %{expand: %%global _httpd_moddir    %%{_libdir}/httpd/modules}}

Summary: Check and update result of previous authentication
Name: mod_auth_fixup
Version: 0.5
Release: 1%{?dist}
License: ASL 2.0
Group: System Environment/Daemons
URL: http://www.adelton.com/apache/mod_auth_fixup/
Source0: http://www.adelton.com/apache/mod_auth_fixup/%{name}-%{version}.tar.gz
BuildRequires: httpd-devel
BuildRequires: pkgconfig
Requires(pre): httpd
Requires: httpd-mmn = %{_httpd_mmn}

# Suppres auto-provides for module DSO per
# https://fedoraproject.org/wiki/Packaging:AutoProvidesAndRequiresFiltering#Summary
%{?filter_provides_in: %filter_provides_in %{_libdir}/httpd/modules/.*\.so$}
%{?filter_setup}

%description
mod_auth_fixup is an authentication module which checks if user
identifier which is the result of previous authentication phases
matches regular expression, optionally updating the identifier.
Another possible use is on Apache 2.2, processing result of
mod_ssl's operation which with SSLVerifyClient require provides
identifier of the user but since mod_ssl is not proper
authentication module, it cannot easily be combined with
authorization modules.

%prep
%setup -q -n %{name}-%{version}

%build
%{_httpd_apxs} -c -Wc,"%{optflags} -Wall -pedantic -std=c99" mod_auth_fixup.c
%if "%{_httpd_modconfdir}" != "%{_httpd_confdir}"
echo > auth_fixup.confx
echo "# Load the module in %{_httpd_modconfdir}/55-auth_fixup.conf" >> auth_fixup.confx
cat auth_fixup.conf >> auth_fixup.confx
%else
cat auth_fixup.module > auth_fixup.confx
cat auth_fixup.conf >> auth_fixup.confx
%endif

%install
rm -rf $RPM_BUILD_ROOT
install -Dm 755 .libs/mod_auth_fixup.so $RPM_BUILD_ROOT%{_httpd_moddir}/mod_auth_fixup.so

%if "%{_httpd_modconfdir}" != "%{_httpd_confdir}"
# httpd >= 2.4.x
install -Dp -m 0644 auth_fixup.module $RPM_BUILD_ROOT%{_httpd_modconfdir}/55-auth_fixup.conf
%endif
install -Dp -m 0644 auth_fixup.confx $RPM_BUILD_ROOT%{_httpd_confdir}/auth_fixup.conf

%files
%doc README LICENSE
%if "%{_httpd_modconfdir}" != "%{_httpd_confdir}"
%config(noreplace) %{_httpd_modconfdir}/55-auth_fixup.conf
%endif
%config(noreplace) %{_httpd_confdir}/auth_fixup.conf
%{_httpd_moddir}/*.so

%changelog
* Thu Apr 23 2015 Jan Pazdziora <jpazdziora@redhat.com> - 0.5-1
- Initial release.
