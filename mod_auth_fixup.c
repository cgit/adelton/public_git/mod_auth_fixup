
/*
 * Copyright 2015 Jan Pazdziora
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "httpd.h"
#include "http_core.h"
#include "http_config.h"
#include "http_log.h"
#include "http_request.h"

#include "ap_regex.h"
#include "apr_strings.h"

typedef struct {
	const char * regexp_str;
	ap_regex_t * regexp;
	const char * substitution_str;
} auth_fixup_config_rec;

static void * create_dir_conf(apr_pool_t * pool, char * dir) {
	auth_fixup_config_rec * cfg
			= apr_pcalloc(pool, sizeof(auth_fixup_config_rec));
	return cfg;
}

static const char * set_regexp(cmd_parms * cmd, void * struct_ptr,
	const char * arg, const char * arg2) {
        auth_fixup_config_rec * cfg = (auth_fixup_config_rec *) struct_ptr;
        if (cfg) {
		cfg->regexp = ap_pregcomp(cmd->pool, arg, AP_REG_EXTENDED);
		if (cfg->regexp == NULL) {
			return "Regular expression could not be compiled.";
		}
		cfg->regexp_str = apr_pstrdup(cmd->pool, arg);
		if (arg2) {
			cfg->substitution_str = apr_pstrdup(cmd->pool, arg2);
		}
        }
        return NULL;
}

static const command_rec auth_fixup_cmds[] = {
	AP_INIT_TAKE12("AuthFixupRegexp", set_regexp, NULL, OR_AUTHCFG,
		"Regular expression to match against r->user, "
		"and optional substitution string with backreferences"),
	{NULL}
};

static int pretend_auth_type(request_rec *r) {
	ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, NULL, "pretend_auth_type: called with r->user [%s]", r->user);
	const char *auth_type = ap_auth_type(r);
	if (! auth_type || strcasecmp(auth_type, "Fixup")) {
		return DECLINED;
	}
	if (r->user) {
		return OK;
	}
	return DECLINED;
}

module AP_MODULE_DECLARE_DATA auth_fixup_module;
static int fixup_user(request_rec *r) {
	ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, NULL, "fixup_user: called with r->user [%s]", r->user);
	if (apr_table_get(r->notes, "fixup-user-already-run")) {
		ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, NULL, "fixup_user: already run in this request, declining");
		return DECLINED;
	}

	auth_fixup_config_rec *conf
		= ap_get_module_config(r->per_dir_config, &auth_fixup_module);
	if (pretend_auth_type(r) == OK) {
		if (!r->user) {
			ap_log_error(APLOG_MARK, APLOG_ERR, 0, NULL,
				"fixup_user: AuthType Fixup configured and no r->user found, forbidden"
				);
			return HTTP_FORBIDDEN;
		}
	}
	if (!r->user) {
		ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, NULL, "fixup_user: declining, no r->user");
		return DECLINED;
	}
	apr_table_setn(r->notes, "fixup-user-already-run", r->user);
	if (!conf->regexp) {
		ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, NULL, "fixup_user: declining, no conf->regexp");
		return DECLINED;
	}

	unsigned int nmatch = AP_MAX_REG_MATCH;
	ap_regmatch_t pmatch[AP_MAX_REG_MATCH];
	if (ap_regexec(conf->regexp, r->user, nmatch, pmatch, 0)) {
		ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, NULL,
			"fixup_user: matching [%s] =~ m/%s/ failed, forbidden",
			r->user, conf->regexp_str);
		return HTTP_FORBIDDEN;
	}

	char * new_user = ap_pregsub(r->pool,
		( conf->substitution_str ? conf->substitution_str : "$0" ),
		r->user, nmatch, pmatch);
	if (! new_user) {
		ap_log_error(APLOG_MARK, APLOG_ERR, 0, NULL,
			"fixup_user: substitution [%s] =~ /%s/ -> [%s] failed, internal server error",
			r->user, conf->regexp_str, conf->substitution_str);
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	if (! *new_user) {
		ap_log_error(APLOG_MARK, APLOG_ERR, 0, NULL,
			"fixup_user: substitution [%s] =~ /%s/ -> [%s] resulted in "
			"empty string, forbidden",
			r->user, conf->regexp_str, conf->substitution_str);
		return HTTP_FORBIDDEN;
	}

	ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, NULL,
		"fixup_user: resetting user [%s] to [%s]", r->user, new_user);
	r->user = new_user;
	return DECLINED;
}


static void register_hooks(apr_pool_t * p) {
#if AP_MODULE_MAGIC_AT_LEAST(20111025,3)
	static const char * const aszSucc[] = {"mod_authz_core.c",
						"mod_lookup_identity.c", NULL};
	static const char * const aszPred[] = {"mod_ssl.c", NULL};
	ap_hook_check_access(fixup_user, aszPred, aszSucc, APR_HOOK_MIDDLE,
					AP_AUTH_INTERNAL_PER_CONF);
	ap_hook_check_authz(fixup_user, NULL, NULL, APR_HOOK_REALLY_FIRST,
					AP_AUTH_INTERNAL_PER_CONF);
	ap_hook_check_user_id(pretend_auth_type, NULL, NULL,
							APR_HOOK_REALLY_FIRST);

#else
	static const char * const aszSucc[] = {"mod_authn_default.c", NULL};
	ap_hook_check_user_id(pretend_auth_type, NULL, aszSucc, APR_HOOK_LAST);
	ap_hook_auth_checker(fixup_user, NULL, NULL, APR_HOOK_REALLY_FIRST);
#endif
}

module AP_MODULE_DECLARE_DATA auth_fixup_module = {
	STANDARD20_MODULE_STUFF,
	create_dir_conf,	/* Per-directory configuration handler */
	NULL,			/* Merge handler for per-directory configurations */
	NULL,			/* Per-server configuration handler */
	NULL,			/* Merge handler for per-server configurations */
	auth_fixup_cmds,	/* Any directives we may have for httpd */
	register_hooks		/* Our hook registering function */
};

